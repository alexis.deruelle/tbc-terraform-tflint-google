output "available_zones" {
  description = "display available zones"
  value       = data.google_compute_zones.available.names
}
