terraform {
  required_version = "~> 1.3"
  required_providers {
    null = {
      source  = "hashicorp/null"
      version = "3.2.1"
    }
    google = {
      source  = "hashicorp/google"
      version = "4.82.0"
    }
  }
}
